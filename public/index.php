<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test de gestion des stagiaires</title>

    <link rel="stylesheet" href="./css/style.css">
</head>

<body>

    <section id="main-container">
        
    <form action="index.php" method="post">
        <input type="text" name="nom"/>
        <input type="text" name="prenom"/>
        <input type="text" name="argent_cantine"/>

    </form>

        <?php

        // Chargement de l'autoloader créé par composer (situé dans le sous dossier "vendor")
        // si vous n'avez pas de sous-dossier "vendor" lancez dans un terminal la commande suivante : "composer install"
        require_once __DIR__ . '/../vendor/autoload.php';

        use Models\Stagiaire;

        // instanciation d'un objet de la classe stagiaire
        $stagiaire1 = new Stagiaire(1, "Lovelace", "Ada");
        $stagiaire2 = new Stagiaire(2, "Stallman", "John");
        $stagiaire3 = new Stagiaire(3, "Gillepsie", "Bobby");
        $stagiaire4 = new Stagiaire(4, "Tob", "Bob");
        $stagiaire5 = new Stagiaire(5, "Teddy", "Bear");

        // Construction du titre avec le prénom - nom de l'objet stagiaire
        echo "<div class='card'>";
        echo "<h2>Bonjour {$stagiaire1->getPrenom()} {$stagiaire1->getNom()}</h2>";
        echo "<label>Argent restant :</label>";
        echo "<p>{$stagiaire1->getArgentCantine()} euros</p>";
        echo "</div>";

        echo "<div class='card'>";
        echo "<h2>Bonjour {$stagiaire2->getPrenom()} {$stagiaire2->getNom()}</h2>";
        echo "<label>Argent restant :</label>";
        echo "<p>{$stagiaire2->getArgentCantine()} euros</p>";
        echo "</div>";

        echo "<div class='card'>";
        echo "<h2>Bonjour {$stagiaire3->getPrenom()} {$stagiaire3->getNom()}</h2>";
        echo "<label>Argent restant :</label>";
        echo "<p>{$stagiaire3->getArgentCantine()} euros</p>";
        echo "</div>";

        echo "<div class='card'>";
        echo "<h2>Bonjour {$stagiaire4->getPrenom()} {$stagiaire4->getNom()}</h2>";
        echo "<label>Argent restant :</label>";
        echo "<p>{$stagiaire4->getArgentCantine()} euros</p>";
        echo "</div>";

        echo "<div class='card'>";
        echo "<h2>Bonjour {$stagiaire5->getPrenom()} {$stagiaire5->getNom()}</h2>";
        echo "<label>Argent restant :</label>";
        echo "<p>{$stagiaire5->getArgentCantine()} euros</p>";
        echo "</div>";

        // $stagiaire->addArgentCantine(12.5);
        ?>

    </section>
</body>

</html>