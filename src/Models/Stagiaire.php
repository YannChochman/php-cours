<?php

namespace Models;

/**
 * Représentation de stagiaires
 */
class Stagiaire
{
    /**
     * Identifiant de l'afpa
     */
    private int $id_afpa;

    /**
     * Le nom d'un stagiaire
     */
    private string $nom;

    /**
     * Le prénom d'un stagaire
     */
    private string $prenom;

    /**
     * L'argent restantn sur le compte cantine d'un stagiaire
     */
    private float $argent_cantine;

    /**
     * Indique si le compte client est activé
     */
    private bool $compte_active;

    /**
     * Le constructeur d'un objet de la classe stagiaire
     */
    public function __construct(int $id_afpa, string $nom, string $prenom)
    {
        $this->id_afpa = $id_afpa;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->argent_cantine = 0.0;
        $this->compte_active = true;
    }

    /**
     * Ajout d'argent sur le compte cantine.
     * Attention, vérification si le compte est activé.
     *
     * @param float $montant Le montant à ajouter sur le compte cantine
     */
    public function addArgentCantine(float $montant): void
    {
        // vérification de l'activaiton du compte && montant est supérieur à 0
        if ($this->compte_active == true && $montant > 0) {
            $this->argent_cantine = $this->argent_cantine + $montant;
        }
    }

    

    /**
     * Get the value of id_afpa
     *
     * @return int
     */
    public function getIdAfpa(): int
    {
        return $this->id_afpa;
    }

    /**
     * Set the value of id_afpa
     *
     * @param int $id_afpa
     *
     * @return self
     */
    public function setIdAfpa(int $id_afpa): self
    {
        $this->id_afpa = $id_afpa;

        return $this;
    }

    /**
     * Get the value of nom
     *
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @param string $nom
     *
     * @return self
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of prenom
     *
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @param string $prenom
     *
     * @return self
     */
    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of argent_cantine
     *
     * @return float
     */
    public function getArgentCantine(): float
    {
        return $this->argent_cantine;
    }

    /**
     * Set the value of argent_cantine
     *
     * @param float $argent_cantine
     *
     * @return self
     */
    public function setArgentCantine(float $argent_cantine): self
    {
        $this->argent_cantine = $argent_cantine;

        return $this;
    }
}
